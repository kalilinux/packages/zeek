#!/bin/sh

# Generate secondary orig tarball for https://github.com/Tencent/rapidjson
#
# Rapidjson's upstream author seems to have given up on the concept of
# properly tagged/released software after the 1.1.0 release in 2016,
# without ever telling anybody.

set -eu

readonly DIR="$1"
readonly VERSION=$(dpkg-parsechangelog -S Version | sed -e 's/-.*$//')
readonly COMMIT=dfbe1db9da455552f7a9ad5d2aea17dd9d832ac1
readonly NAME="rapidjson-$COMMIT"
readonly TARBALL="../zeek_$VERSION.orig-rapidjson.tar.xz"

( cd "$DIR" && git archive --format=tar --prefix="$NAME/" "$COMMIT" ) \
    | tar -f - \
	  --delete "$NAME/bin/" \
	  --delete "$NAME/contrib/" \
	  --delete "$NAME/docker/" \
	  --delete "$NAME/doc/" \
	  --delete "$NAME/example/" \
	  --delete "$NAME/test/" \
	  --delete "$NAME/thirdparty/" \
    | xz -9 \
> "$TARBALL"
rm -rf rapidjson/
mkdir rapidjson/
tar -C rapidjson/ --strip=1 -xJf "$TARBALL"
